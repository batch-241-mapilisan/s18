function add(x, y) {
    console.log(`Display sum of ${x} and ${y}`);
    console.log(x + y);
}

function subtract(x, y) {
    console.log(`Displayed difference of ${x} and ${y}`);
    console.log(x - y);
}

add(5, 15);
subtract(20, 5);

function multiply(x, y) {
    return x * y;
} 

function divide(x, y) {
    return x / y;
}

let product = multiply(50, 10);
let quotient = divide(50, 10);
console.log(product);
console.log(quotient);

function getAreaOfCircle(radius) {
    const area = 3.14 * (radius ** 2);
    return area;
}

let circleArea = getAreaOfCircle(15);
console.log(circleArea);

function getAverage(num1, num2, num3, num4) {
    const average = (num1 + num2+ num3 + num4) / 4;
    return average;
}

let averageVar = getAverage(20, 40, 60, 80);

function checkIfPassOrFail(score, totalScore) {
    const percentage = (score / totalScore) * 100;
    const isPassed = percentage > 75;
    return isPassed;
}

let isPassingScore = checkIfPassOrFail(38, 50);

